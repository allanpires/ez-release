# EZ Release

## About
Once upon a time there was a dev who had to read all commits merged into master in the last two weeks so he could write a changelog.
He hated it.

## Configuration

Create a OAuth consumer on BitBucket: https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html

Create a .ENV file and replace the fields wanted:
```
OAUTH_KEY=gabrielSaidThatBitbucketSucks
OAUTH_SECRET=andIThinkItsTrue

BITBUCKET_ACCESS_TOKEN_URL=https://bitbucket.org/site/oauth2/access_token
BITBUCKET_PULL_REQUESTS_URL=https://api.bitbucket.org/2.0/repositories
BITBUCKET_USER=MY_BITBUCKET_USER
BITBUCKET_REPOSITORY=MY_BITBUCKET_REPO

DEFAULT_STATE=merged
DEFAULT_BRANCH=master
DEFAULT_SPRINT_LENGTH=14
```

## Run it

You can run the project using this command:
`ruby main.rb`

If you want you can pass the state, branch and days as parameter:

`ruby main.rb [STATE] [BRANCH] [START_DATE] [END_DATE]`

`ruby main.rb open qa 01-08-2018`

`ruby main.rb open qa 01-08-2018 31-10-2018`

If there are no params, default values from .ENV file will be used.