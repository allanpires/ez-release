require 'net/https'
require 'logger'
require 'dotenv/load'
require 'json'
require 'date'
require 'uri'

class RequestHandler
  LOGGER = Logger.new(STDOUT)

  attr_accessor :token
  attr_accessor :changelog

  def initialize
    @token = bitbucket_authorization_token
    @changelog = ""
  end

  def pull_requests_info(state, branch, start_date, end_date)
    url = create_pull_request_info_url(state, branch, start_date, end_date)
    response = request_bitbucket_api(url)
    response_body = JSON.parse(response.body)
    total_pages = response_body["size"].to_i
    current_page = 0
    LOGGER.info("[PULL_REQUEST_INFO] total_pages = #{total_pages + 1}")

    while (current_page <= total_pages)
      response = next_page(url, current_page)
      response_body = JSON.parse(response.body)
      response_body['values'].each do |pull_request|
        @changelog = @changelog + "* #{pull_request['title']}\n"
      end
      current_page = current_page + 1
    end

    return @changelog
  end

  def request_bitbucket_api(url)
    uri = URI.parse(url)
    req = Net::HTTP::Get.new(uri)
    req['Authorization'] = "Bearer #{@token}"
    req_options = { use_ssl: uri.scheme == "https" }

    http = Net::HTTP.start(uri.hostname, uri.port, req_options)
    response = http.request(req)

    return response
  end

  def next_page(url, current_page)
    LOGGER.info("[PULL_REQUEST_INFO] current_page = #{current_page+1}")
    next_page_url = url + "&page=#{current_page+1}"
    request_bitbucket_api(next_page_url)
  end

  private
  def bitbucket_authorization_token
    uri = URI.parse(ENV['BITBUCKET_ACCESS_TOKEN_URL'])
    LOGGER.info("[TOKEN_REQUEST] uri = #{uri}")

    request = Net::HTTP::Post.new(uri)
    request.basic_auth(ENV['OAUTH_KEY'], ENV['OAUTH_SECRET'])
    request.set_form_data("grant_type" => "client_credentials")
    req_options = { use_ssl: uri.scheme == "https" }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end
    LOGGER.info("[TOKEN_REQUEST] response_code = #{response.code}")

    response_body = JSON.parse(response.body)
    return response_body["access_token"]
  end

  def create_pull_request_info_url(state, branch, start_date, end_date)
    uri = URI.parse("#{ENV['BITBUCKET_PULL_REQUESTS_URL']}/#{ENV['BITBUCKET_USER']}/#{ENV['BITBUCKET_REPOSITORY']}/pullrequests")
    query_string = 'state = "'+state+'" AND destination.branch.name = "'+branch+'"' + 'AND created_on >= ' + start_date.to_s + 'AND created_on <= ' + end_date.to_s
    params = { q: query_string }
    uri.query = URI.encode_www_form(params)

    return uri.to_s
  end

end
