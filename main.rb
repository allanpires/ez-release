#!/usr/bin/env ruby
require_relative 'request_handler'
require_relative 'file_handler'

LOGGER = Logger.new(STDOUT)

def state
  return ARGV[0] unless ARGV[0].nil?
  ENV['DEFAULT_STATE']
end

def branch
  return ARGV[1] unless ARGV[1].nil?
  ENV['DEFAULT_BRANCH']
end

def start_date
  return Date.parse(ARGV[2]) unless ARGV[2].nil?
  Date.today - ENV['DEFAULT_SPRINT_LENGTH'].to_i
end

def end_date
  return Date.parse(ARGV[3]) unless ARGV[3].nil?
  Date.today
end

def main
  LOGGER.info("[PARAMS] state = #{state}, branch = #{branch}, start_date = #{start_date}, end_date = #{end_date}")

  requester = RequestHandler.new
  changelog = requester.pull_requests_info(state, branch, start_date, end_date)
  FileHandler.save_to_file("changelogs/changelog-#{branch}-#{state}-#{start_date}-#{end_date}.txt", changelog)
end

main
