require 'logger'
require 'fileutils'

class FileHandler
  
  def self.save_to_file(name, content)
    create_directory(name)
    out_file = File.new(name, "w")
    out_file.puts(content)
    out_file.close
  end

  def self.create_directory(path)
    dirname = File.dirname(path)
    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end
  end
end
